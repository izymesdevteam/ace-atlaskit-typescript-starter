# Atlassian Connect Starter App using Express and Atlaskit via TypeScript

Multi-component marriage of Atlaskit and Atlassian Connect Express utilising Typescript on the front end

## What's next?

[Read the docs](https://bitbucket.org/atlassian/atlassian-connect-express/src/master/README.md).
