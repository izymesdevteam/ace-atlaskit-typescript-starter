import * as React from 'react';
export default () => (
  <section style={{marginBottom: '10px'}}>
    <p>
      Multi-component marriage of Atlaskit and Atlassian Connect Express utilising Typescript on the front end.
    </p>
    <p>
      Provides a working navigation component. See /src/components/StarterNavigation.js.
    </p>
  </section>
);
