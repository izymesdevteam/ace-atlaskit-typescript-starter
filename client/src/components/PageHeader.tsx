import React from 'react';
import { BreadcrumbsItem, BreadcrumbsStateless } from '@atlaskit/breadcrumbs';
import PageHeader from '@atlaskit/page-header';

const breadcrumbs = (
    <BreadcrumbsStateless onExpand={() => {}}>
        <BreadcrumbsItem text="AI Insights" key="AI Insights" />
        <BreadcrumbsItem text="Parent page" key="Parent page" />
    </BreadcrumbsStateless>
);

export default () => (
    <PageHeader breadcrumbs={breadcrumbs}>
        AI Insights for Jira Service Desk
    </PageHeader>
);
