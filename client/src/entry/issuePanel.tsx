import React from 'react';
import ReactDOM from 'react-dom';
// @ts-ignore
import IssuePanel from '../modules/IssuePanel';

// eslint-disable-next-line no-undef
export const render = () => {
    ReactDOM.render(<IssuePanel />, document.getElementById('issue-panel-root'));
};