import React from 'react';
import ReactDOM from 'react-dom';
import PageHeader from './modules/MainRouter';

// eslint-disable-next-line no-undef
export const render = () => {
  ReactDOM.render(<PageHeader />, document.getElementById('app-root'));
};
