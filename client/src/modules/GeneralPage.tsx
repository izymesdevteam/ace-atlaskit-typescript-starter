import PropTypes from 'prop-types';
import React, { Component } from 'react';
import Flag, { FlagGroup } from '@atlaskit/flag';
import SuccessIcon from '@atlaskit/icon/glyph/check-circle';
import Modal from '@atlaskit/modal-dialog';
import Lorem from 'react-lorem-component';
import Page from '@atlaskit/page';
import StarterNavigation from '../components/StarterNavigation';
import date from "@atlaskit/icon/glyph/editor/date";
import {UIAnalyticsEvent} from "@atlaskit/analytics-next";
interface FlagType {
    id: number
}
interface State {
    flags: FlagType[];
    isModalOpen: boolean;
}

export default class GeneralPage extends Component<any,State> {
    state = {
        flags: [],
        isModalOpen: false,
    };

    static contextTypes = {
        navOpenState: PropTypes.object,
        router: PropTypes.object,
    };

    static propTypes = {
        navOpenState: PropTypes.object,
        onNavResize: PropTypes.func,
    };

    static childContextTypes = {
        showModal: PropTypes.func,
        addFlag: PropTypes.func,
    }

    getChildContext() {
        return {
            showModal: this.showModal,
            addFlag: this.addFlag,
        };
    }

    showModal = () => {
        this.setState({ isModalOpen: true });
    }

    hideModal = () => {
        this.setState({ isModalOpen: false });
    }

    addFlag = () => {
        this.setState({ flags: [{ id: Date.now() }].concat(this.state.flags) });
    }

    onFlagDismissed = (dismissedFlagId:number | string, analyticsEvent: UIAnalyticsEvent) => {
        this.setState({
            flags: this.state.flags.filter((flag:FlagType) => flag.id !== dismissedFlagId),
        })
    }

    render() {
        console.log('Entering generalPage', this.props, this.state);
        return (
            <div>
                <Page
                    navigation={<StarterNavigation  onNavResize={()=>void 0}/>}
                >
                    {this.props.children}
                </Page>
                <div>
                    <FlagGroup onDismissed={this.onFlagDismissed}>
                        {
                            this.state.flags.map((flag:FlagType) => (
                                <Flag
                                    id={flag.id}
                                    key={flag.id}
                                    title="Izymes Flag"
                                    description="Lets get this party started..."
                                    icon={<SuccessIcon label="Info" />}
                                />
                            ))
                        }
                    </FlagGroup>
                    {
                        this.state.isModalOpen && (
                            <Modal
                                heading="Izymes Cloud Dev"
                                actions={[{ text: 'Exit Modal', onClick: this.hideModal }]}
                                onClose={this.hideModal}
                            >
                                <Lorem count={2} />

                            </Modal>
                        )
                    }
                </div>
            </div>
        );
    }
}
