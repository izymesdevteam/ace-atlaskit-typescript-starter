import * as PropTypes from 'prop-types';
import * as React from 'react';
import { Router, Route, browserHistory } from 'react-router';
import GeneralPage from './GeneralPage';
import HomePage from '../pages/HomePage';
import SettingsPage from '../pages/SettingsPage';

export default class MainRouter extends React.Component<{},NavOpenState> {

  constructor(props: Readonly<any>) {
    super(props);
    this.state = {
      navOpenState: {
        isOpen: true,
        width: 304,
      }
    }
  }
  
  getChildContext () {
    return {
      navOpenState: this.state.navOpenState,
    };
  }

  appWithPersistentNav = () => (props:any) => {
    console.log('Entering main appWithPersistentNav', props);
    return <GeneralPage
      onNavResize={this.onNavResize}
      {...props}
    />
  }

  onNavResize = (navOpenState:NavOpenState) => {
    this.setState(
      navOpenState
    );
  }

  render() {
    console.log('Entering main router');
    return (
      <Router history={browserHistory}>
        <Route component={this.appWithPersistentNav()}>
          <Route path="/general-page" component={HomePage} />
          <Route path="/settings" component={SettingsPage} />
        </Route>
      </Router>
    );
  }
}

// @ts-ignore
MainRouter.childContextTypes = {
  navOpenState: PropTypes.object,
}
