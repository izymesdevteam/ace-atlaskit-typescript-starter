declare module 'react-lorem-component'
declare module 'styled-components'
declare module 'prop-types'
declare module '@atlaskit/navigation'
declare module 'react-router-dom'
declare module 'react-router'

interface NavOpenState{
    navOpenState: {
        isOpen: boolean;
        width: number;
    }
}
interface StarterNavigationProps {
    onNavResize: ()=>void;
}