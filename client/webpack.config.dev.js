'use strict';

const webpack = require('webpack');

module.exports = {
    mode: 'development',
    entry: {
        generalPage: './src/entry/generalPage.tsx',
        issuePanel: './src/entry/issuePanel.tsx'
    },
    output: {
        path: __dirname + "/../public/js/",
        filename: '[name].bundle.js',
        library: '[name]'
    },
    watchOptions: {
        aggregateTimeout: 600,
        ignored: /node_modules/
    },
    plugins: [
        new webpack.DefinePlugin({
            "process.env": {
                NODE_ENV: JSON.stringify("development")
            }
        })
    ],
    resolve: {
        extensions: ['.js', '.tsx', '.ts', '.jsx', '.css', '.scss', '.less']
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                exclude: /(node_modules)/,
                use: {
                    loader: 'ts-loader'
                }
             },
            {
                test: /\.m?js$/,
                exclude: /(node_modules)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            },
            {
                test: /\.css$/,
                use: [ 'style-loader', 'css-loader', 'autoprefixer-loader']
            },
            {
                test: /\.svg$/,
                use: [{
                    loader: 'svg-inline-loader',
                    options: {
                    publicPath: '/js'
                    }
                }],
                exclude: /node_modules/
            },
            {
                test: /\.png$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        publicPath: '/js'
                    }
                }],
                exclude: /node_modules/
            },
            {
                test: /\.scss$/,
                use: {
                    loader: 'style!css!sass',
                }
            },
            {
                test: /\.less$/,
                use: {
                    loader: 'style!css!less',
                }
            }
        ]
    }
};
