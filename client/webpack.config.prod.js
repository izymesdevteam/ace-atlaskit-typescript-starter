'use strict';
const webpack = require('webpack');
module.exports = {
    mode: 'production',
    entry: {
        generalPage: './src/entry/generalPage.tsx',
        issuePanel: './src/entry/issuePanel.tsx'
    },
    output: {
        path: __dirname + "/../public/js/",
        filename: '[name].bundle.js',
        library: '[name]'
    },
    optimization: {
        minimize: true
    },
    plugins: [
        new webpack.DefinePlugin({
            "process.env": {
                NODE_ENV: JSON.stringify("production")
            }
        }),
    ],
    resolve: {
        extensions: ['.js', '.tsx', '.ts', '.jsx', '.css', '.scss', '.less']
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                exclude: /(node_modules)/,
                use: {
                    loader: 'ts-loader'
                }
            },
            {
                test: /\.m?js$/,
                exclude: /(node_modules)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            },
            {
                test: /\.svg$/,
                use: [{
                    loader: 'svg-inline-loader',
                    options: {
                        publicPath: '/js'
                    }
                }],
                exclude: /node_modules/
            },
            {
                test: /\.png$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        publicPath: '/js'
                    }
                }],
                exclude: /node_modules/
            },
            {
                test: /\.css$/,
                use: [ 'style-loader', 'css-loader', 'autoprefixer-loader']
            },
            {
                test: /\.scss$/,
                use: {
                    loader: 'style!css!sass',
                }
            },
            {
                test: /\.less$/,
                use: {
                    loader: 'style!css!less',
                }
            }
        ]
    }
};
